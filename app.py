from flask import Flask, render_template, request
import pickle
import pandas as pd
app = Flask(__name__)
from io import BytesIO
import base64

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

model = pickle.load(open("model.pkl","rb"))

df2 = pd.read_csv('Preprocessed1.csv')
df2 =df2.drop(['Date','year','month','day','day_of_week'], axis=1)

#defining column name
cols = ['Open', 'High', 'Volume']

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/home")
def home():
    return render_template('home.html')

@app.route("/predict", methods=['POST'])
def predict():
    if request.method == 'POST':
        open = float(request.form['open'])
        high = float(request.form['high'])
        volume = float(request.form['volume'])
        
        date = request.form['date']

        # Create a DataFrame with date, A1, A2, A3
        data = {'Date': [date], 'Open': [open], 'High': [high], 'Volume': [volume]}
        df = pd.DataFrame(data)

        df['Date'] = pd.to_datetime(df['Date'])
        df.set_index('Date', inplace=True)

        # Extract features for prediction
        x = df[['Open', 'High', 'Volume']].values

        # Make predictions using the trained stacking model
        df['Predicted_Close'] = model.predict(x)

        # Yearly predictions
        df['Month'] = df.index.to_period('M')
        monthly_predictions = df.groupby('Month')['Predicted_Close'].mean()

        # Yearly predictions
        df['Year'] = df.index.to_period('Y')
        yearly_predictions = df.groupby('Year')['Predicted_Close'].mean()

        # Graphs Nigga
        ma100 = df2.Close.rolling(100).mean()
        ma200 = df2.Close.rolling(200).mean()

        # Plotting
        fig, ax = plt.subplots(figsize=(12, 6))
        ax.plot(df2.index, df2['Close'], label='Close')
        ax.plot(df2.index, ma100, 'r', label='MA100')
        ax.plot(df2.index, ma200, 'g', label='MA200')
        ax.legend()

        # Convert plot to base64 encoded string
        buffer = BytesIO()
        plt.savefig(buffer, format='png')
        buffer.seek(0)
        plot_data = base64.b64encode(buffer.getvalue()).decode('utf-8')
        plt.close()

        return render_template("output.html", yearly_predictions=yearly_predictions, monthly_predictions=monthly_predictions, plot_data=plot_data)
    
    

